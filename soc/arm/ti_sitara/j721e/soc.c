#include <stdint.h>

#include <zephyr/kernel.h>
#include <zephyr/init.h>
#include <zephyr/logging/log.h>
#include <zephyr/fatal.h>

#include "ipc_rpmsg_linux_resource_table.h"

#define DebugP_MEM_LOG_SIZE 1024

__attribute__((section (".log_shared_mem"))) char volatile gDebugMemLog[DebugP_MEM_LOG_SIZE];

const RPMessage_ResourceTable gRPMessage_linuxResourceTable __attribute__ ((section (".resource_table"), aligned (4096))) =
{
    {
        1U,         /* we're the first version that implements this */
        2U,         /* number of entries, MUST be 2 */
        { 0U, 0U, } /* reserved, must be zero */
    },
    /* offsets to the entries */
    {
        offsetof(RPMessage_ResourceTable, vdev),
        offsetof(RPMessage_ResourceTable, trace),
    },
    /* vdev entry */
    {
        RPMESSAGE_RSC_TYPE_VDEV, RPMESSAGE_RSC_VIRTIO_ID_RPMSG,
        0U, 1U, 0U, 0U, 0U, 2U, { 0U, 0U },
    },
    /* the two vrings */
    { RPMESSAGE_RSC_VRING_ADDR_ANY, 4096U, 256U, 1U, 0U },
    { RPMESSAGE_RSC_VRING_ADDR_ANY, 4096U, 256U, 2U, 0U },
    {
        (RPMESSAGE_RSC_TRACE_INTS_VER0 | RPMESSAGE_RSC_TYPE_TRACE),
        (uint32_t)gDebugMemLog, DebugP_MEM_LOG_SIZE,
        0, "trace:r5fss0_0",
    },
};

static int j721e_init(void)
{
  return 0;
}

uint32_t sys_clock_elapsed(void)
{
  return 0;
}

void z_soc_irq_init(void)
{
}

void z_soc_irq_enable(unsigned int irq)
{
}

void z_soc_irq_disable(unsigned int irq)
{
}

int z_soc_irq_is_enabled(unsigned int irq)
{
  return 0;
}

void z_soc_irq_priority_set(unsigned int irq, unsigned int prio, unsigned int flags)
{
}

unsigned int z_soc_irq_get_active(void)
{
  return CONFIG_NUM_IRQS + 1;
}

void z_soc_irq_eoi(unsigned int irq)
{
}

SYS_INIT(j721e_init, PRE_KERNEL_1, CONFIG_KERNEL_INIT_PRIORITY_DEFAULT);
